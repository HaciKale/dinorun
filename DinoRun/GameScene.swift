//
//  GameScene.swift
//  DinoRun
//
//  Created by Haci Kale on 12/03/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//


import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate
{
    var dino = SKSpriteNode()
    var food = SKSpriteNode()
    let dinoGroup:UInt32=0
    let objectGroup:UInt32=1
    let foodGroup:UInt32=2
    let boxGroup:UInt32=4
    var scoreLabel = SKLabelNode(fontNamed: "Funky Stoneage")
    var score=0
    var movingObjects = SKNode()
    var gameOverLabel = SKLabelNode(fontNamed: "Funky Stoneage")
    var gameOver=0

    
    override func didMoveToView(view: SKView)
    {
        /* Setup your scene here */
        physicsWorld.gravity = CGVectorMake(0, -8.0)
        physicsWorld.contactDelegate = self
        
        addChild(movingObjects)
        createBackground()
        createDino()
        Ground()
        loft()
        
        //create Score label
        scoreLabel.fontSize=130
        scoreLabel.position=CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)+450)
        scoreLabel.text="Score:\(score)"
        scoreLabel.zPosition=140
        addChild(scoreLabel)
        
        //create game over label
        gameOverLabel.fontSize=300
        gameOverLabel.fontColor = SKColor.brownColor()
        gameOverLabel.position=CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)+100)
        gameOverLabel.zPosition=140
        
        
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([SKAction.runBlock(spawnFood),
                SKAction.waitForDuration(3)])))
        
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([SKAction.runBlock(createBox),
                SKAction.waitForDuration(5)])))
        
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([SKAction.runBlock(createCloud),
                SKAction.waitForDuration(15)])))
        }

    
    func didBeginContact(contact: SKPhysicsContact)
    {
        //this gets called automatically when two objects begin contact with each other
        
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        switch(contactMask) {
            
        case boxGroup | dinoGroup:
            //either the contactMask was the BOX type or the Dino type

            movingObjects.removeFromParent()
            dino.removeFromParent()
            
            gameOverLabel.text="Game Over"
            addChild(gameOverLabel)
            
            
            
            
        case foodGroup | dinoGroup:
            //either the contactMask was the food type or the dino type
            
            score++
            scoreLabel.text="Score: \(score)"
            destroyFood(contact.bodyB.node as SKSpriteNode)

            
        default:
            return
            
        }
     
    }
    
    
    func destroyFood(food: SKSpriteNode)
    {
       
        food.removeFromParent()
    }
    func Ground()
    {
        var ground = SKNode()
        ground.position=CGPoint(x: 0, y: 306)
        ground.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: size.width*4, height: 2))
        ground.physicsBody?.dynamic = false
        ground.physicsBody?.categoryBitMask=objectGroup
        
        self.addChild(ground)
    }
    
    func loft()
    {
        var loft = SKNode()
        loft.position=CGPoint(x: 0, y: 1256)
        loft.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: size.width*4, height: 2))
        loft.physicsBody?.dynamic = false
        loft.physicsBody?.categoryBitMask=objectGroup
        
        self.addChild(loft)
    }
    
    
    func createBackground()
    {
        var backgroundTexture = SKTexture(imageNamed: "bg4.png")
        var background = SKSpriteNode(texture: backgroundTexture)
        background.position = CGPoint(x: 0 + backgroundTexture.size().width , y:CGRectGetMidY(self.frame))
        background.speed = 1
        background.setScale(3.5)
        background.zPosition = -1 /*gør at baggrunden er bag alt*/
        addChild(background)
        
    }
    
    
    func createCloud()
    {
        var cloudTexture = SKTexture(imageNamed: "clouds.png")
        var cloud = SKSpriteNode(texture: cloudTexture)
        
        cloud.position = CGPoint(x:size.width + cloud.size.width/2, y:1000)
        cloud.setScale(3.5)
        movingObjects.addChild(cloud)
        
        let movecloud = SKAction.moveToX(-size.width * 2 , duration: 20)
        let removecloud = SKAction.removeFromParent()
        let moveAndRemove = SKAction.sequence([movecloud, removecloud])
        cloud.runAction(moveAndRemove)
        
       
    }
 

    func spawnFood()
    {
        // 1
        food = SKSpriteNode(imageNamed: "egg")
        food.name = "food"
        food.physicsBody = SKPhysicsBody(circleOfRadius: food.size.height/3)
        food.physicsBody?.categoryBitMask = foodGroup
        food.physicsBody?.contactTestBitMask = dinoGroup
        food.physicsBody?.collisionBitMask = boxGroup
        food.physicsBody?.dynamic = false
        
        food.position = CGPoint(x:size.width + food.size.width/2, y:400)
        movingObjects.addChild(food)
        // 2
        var appear = SKAction.scaleTo(0.7, duration: 0.5)
        
        food.zRotation = -3.14/16.0
        var leftWiggle = SKAction.rotateByAngle(3.14/8.0, duration: 0.5)
        var rightWiggle = leftWiggle.reversedAction()
        var fullWiggle = SKAction.sequence([leftWiggle, rightWiggle])
        var scaleUp = SKAction.scaleBy(1.2, duration: 0.25)
        var scaleDown = scaleUp.reversedAction()
        var fullScale = SKAction.sequence(
            [scaleUp, scaleDown, scaleUp, scaleDown])
        
        var group = SKAction.group([fullScale, fullWiggle])
        var groupWait = SKAction.repeatAction(group, count: 10)
        var disappear = SKAction.scaleTo(0, duration: 0.5)
        var removeFromParent = SKAction.removeFromParent()
        var actions = [appear, groupWait, disappear, removeFromParent]
        food.runAction(SKAction.sequence(actions))
    
        var movefood = SKAction.moveToX(-size.width * 2 , duration: 12)
        var removefood = SKAction.removeFromParent()
        var moveAndRemove = SKAction.sequence([movefood, removefood])
        food.runAction(moveAndRemove)
        
        
        
    }

    

    func createBox()
    {
        let box = SKSpriteNode(imageNamed: "box2p")
        box.name = "box"
        box.position = CGPoint(x:size.width + box.size.width/2, y: 460)
        box.setScale(0.3)
        movingObjects.addChild(box)
   
        
        let moveBox = SKAction.moveToX(-box.size.width * 2 , duration: 4.0)
        let removeBox = SKAction.removeFromParent()
        let moveAndRemove = SKAction.sequence([moveBox, removeBox])
        box.runAction(moveAndRemove)
        
        box.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: box.size.width, height: box.size.height))
        box.physicsBody?.categoryBitMask = boxGroup
        box.physicsBody?.contactTestBitMask = dinoGroup
        box.physicsBody?.collisionBitMask = foodGroup
        box.physicsBody?.dynamic = false
    }
    
    
    

    
    func createDino()
    {
        
        /*this is the dino*/
        var dinoTex6 = SKTexture(imageNamed: "1.png")
        var dinoTex5 = SKTexture(imageNamed: "2.png");
        var dinoTex4 = SKTexture(imageNamed: "3.png");
        var dinoTex3 = SKTexture(imageNamed: "4.png");
        var dinoTex2 = SKTexture(imageNamed: "5.png");
        var dinoTex1 = SKTexture(imageNamed: "6.png");
        dino = SKSpriteNode(texture: dinoTex1)
        
        /*this is what makes the dino switch between 2 looks so that the wings move*/
        var alternateTexture = SKAction.animateWithTextures([dinoTex1, dinoTex2, dinoTex3, dinoTex4, dinoTex5,dinoTex6], timePerFrame: 0.05)
        var repeatForever = SKAction.repeatActionForever(alternateTexture)
        
        dino.position = CGPoint(x: 340, y: 350)
       
        dino.setScale(1.5)
        /*Give the dino a positive zPosition to ensure it's in the foreground*/
        dino.zPosition = 100
        dino.physicsBody = SKPhysicsBody(circleOfRadius: dino.size.height/2)
        dino.physicsBody?.categoryBitMask = dinoGroup
        dino.physicsBody?.collisionBitMask = objectGroup
        dino.physicsBody?.contactTestBitMask = boxGroup | foodGroup
        
        dino.physicsBody?.allowsRotation = false
        dino.physicsBody?.angularVelocity = 0
        /*Run the repeatForever action on the dino*/
        dino.runAction(repeatForever)
        
        /*Add the dino as a child to the GameScene*/
        addChild(dino)
    }
    
  
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent)
    {
        
            dino.physicsBody?.applyImpulse((CGVectorMake(0, 2000)));
    }
    
    
 

    override func update(currentTime: CFTimeInterval)
    {
        
    }
}
