//
//  GameOverScene.swift
//  DinoRun
//
//  Created by Haci Kale on 20/03/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//

import Spritekit
class GameOverScene: SKScene
{
    init(size: CGSize, won: Bool)
    {
        super.init(size: size)
        var imageUrl = ""
        if won
        {
            imageUrl = "YouWin"
        }
        else
        {
            imageUrl = "YouLose"
        }
        var background=SKSpriteNode(imageNamed: imageUrl)
        background.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame))
        addChild(background)
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    override func didMoveToView(view: SKView)
    {
        
    }
}
